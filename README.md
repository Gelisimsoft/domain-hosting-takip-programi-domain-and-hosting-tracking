# Domain ve Hosting Takip Programı

![Alt text](https://raw.githubusercontent.com/Gelisimsoft/DomainveHostingTakipProgrami-CSharp-/master/DomainveHostingTakipProgrami.png)

## GİRİŞ BİLGİLERİ ##

Kullanıcı adı	: admin

Şifre		: demo 

## MODÜLLER ##

1. Müşteri Kayıt
2. Hosting Kayıt
3. Domain Kayıt
4. E-posta Kayıt
5. Tahsilat Kayıt
6. Müşteri Raporlar
7. Hosting Raporlar
8. Domain Raporlar
9. E-posta Raporlar
10. Süresi Bitenler Raporları
11. Kullanıcı İşlemleri


www.gelisimsoft.com 

info@gelisimsoft.com
